const fs = require("fs");
const path = require("path");
// js文件压缩
class Analyze {
    /**
     * @param {Object} options 
     */
    constructor(options) {
        his.option = Object.assign({ delMComment: true, delSComment: true, addSemicolon: true, compress: true }, option || {});
        this.reg_multi_comment = /\s*\/\*[\s\S]*?\*\//gm; //多行注释正则匹配
        this.reg_single_comment = /\s*\/\/[.\.]*(?:\r)(\n)$/g;//单行注释正则匹配
        this.reg_blank_line = /(\n|\r\n){1,}/g; //空行正则匹配
        this.end_semicolon = /;$/g; // 添加分号
        this.solidity = /(\n|\r\n|\t| {2,})*/g;
        this.getInputFile();
        this.createDir();
    }

    getInputFile() {
        let inputDir = "", inputFile = ""; //定义基本路径
        if (path.isAbsolute(this.option.inputDir)) {
            inputDir = this.option.inputDir;
        } else {
            inputDir = path.join(__dirname, this.option.inputDir);
        }
        inputFile = path.join(inputDir, this.option.inputFile);
        if (fs.existsSync(inputFile)) {
            this.data = fs.readFileSync(inputFile).toString();
            if (!this.data && typeof this.data !== 'string') {
                throw new Error('Cannot Analyze null and non-string')
            } else {
                this.parse();
            }

        } else {
            console.error(`要处理的文件${inputFile}不存在,请检查文件的路径是否正确`);
        }
    }

    createDir() {
        let outputDir = ""; //定义基本路径
        if (path.isAbsolute(this.option.outputDir)) {
            outputDir = this.option.outputDir;
        } else {
            outputDir = path.join(__dirname, this.option.outputDir);
        }
        if (fs.existsSync(outputDir)) {
            console.log(`目录${outputDir}已存在`)
        } else {
            console.log(`目录${outputDir}不存在`);
            fs.mkdirSync(outputDir);
            if (fs.existsSync(outputDir)) {
                console.log(`目录${outputDir}创建成功`);
                this.option.outputDir = outputDir;
            } else {
                console.error(`目录${outputDir}创建失败`);
                return;
            }
        }
        this.createFile();
    }

    createFile() {
        this.option.outputFile = path.join(this.option.outputDir, this.option.outputFile);
        if (fs.existsSync(this.option.outputFile)) {
            console.log(`文件${this.option.outputFile}已存在,文件将被强制重写`)
        } else {
            console.log(`文件${this.option.outputFile}不存在`);
        }
        let result = fs.writeFileSync(this.option.outputFile, this.data);
        if (!result) {
            console.log(`文件${this.option.outputFile}写入成功`);
        } else {
            throw new Error(result);
        }
    }

    delMComment() {
        this.data = this.data.replace(this.reg_multi_comment, '');
        return this
    }

    /**
     * 删除单行注释
     */
    delSComment() {
        this.data = this.data.replace(this.reg_single_comment, '');
        return this
    }


    delBLine() {
        this.data = this.data.replace(this.reg_blank_line, '').trim();
        return this
    }

    addSemicolon() {
        let result;
        while ((result = this.end_semicolon.exec(this.data)) !== null) {
            console.log(result)
        }
        // this.data = this.data.replace(this.end_semicolon, function (match, p1) { return match.replace(p1, p1 + ';') });
        return this
    }


    compress() {
        this.data = this.data.replace(this.solidity, '');
        return this
    }

    parse() {
        if (this.option.delMComment) {
            console.log("删除多行注释")
            this.delMComment();
        }
        if (this.option.delSComment) {
            console.log("删除单行注释")
            this.delSComment();
        }
        if (this.option.addSemicolon) {
            console.log("函数末尾添加分号");
            this.addSemicolon();
        }
        if (this.option.compress) {
            console.log("文件压缩")
            this.compress();
        }
        return this.data
    }
}